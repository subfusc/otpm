A simple Ruby cli and library for storing and generating secrets.

[![pipeline status](https://gitlab.com/subfusc/otpm/badges/master/pipeline.svg)](https://gitlab.com/subfusc/otpm/commits/master)

This is a simple library with a cli program to store otpm secrets in an encrypted
database for storage on a computer.

Requirements:
 - Ruby >= 2.3
 - ROTP
 - OpenSSL
 - YAML